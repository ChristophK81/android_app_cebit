package com.q_con.ck.qdetector;

import java.io.File;
import java.util.List;
import java.util.LinkedList;
import java.util.ArrayList;
import org.opencv.core.CvType;
import org.opencv.core.Scalar;

import org.opencv.core.Mat;

import org.opencv.core.MatOfDMatch;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.features2d.DescriptorMatcher;
import org.opencv.features2d.FeatureDetector;
import org.opencv.features2d.DescriptorExtractor;
import org.opencv.features2d.Features2d;
import org.opencv.features2d.KeyPoint;
import org.opencv.features2d.DMatch;
import org.opencv.highgui.Highgui; 
import org.opencv.core.MatOfPoint2f;
import org.opencv.imgproc.Imgproc;
import org.opencv.core.Point;
import org.opencv.core.MatOfByte;
import org.opencv.calib3d.Calib3d;
;
import android.os.Environment;
import android.util.Log;

public class Detector {

    private static final String TAG = "qDetector";


    private FeatureDetector mDetector ;
	private DescriptorExtractor mExtractor;
	private DescriptorMatcher mMatcher;
	
	
	private List<Mat> mLogos;
	private Mat mFrame;
	
	private List<MatOfKeyPoint> mokLogos;
	private MatOfKeyPoint mokFrame;
	
	private List<Mat> descriptorLogos;
	private Mat descriptorFrame;
	
	double DISTANCE_FAKTOR = 2;
	
	boolean DEBUG = true;
    boolean DEBUG_FILE = false;
	String DEBUG_PATH = "/";
	
	public Detector(){
		mDetector = FeatureDetector.create(FeatureDetector.ORB);
		mExtractor = DescriptorExtractor.create(DescriptorExtractor.ORB);
		mMatcher= DescriptorMatcher.create(DescriptorMatcher.BRUTEFORCE_HAMMINGLUT);
	}

    void logMessage(int msg){
        logMessage( String.format("%d", msg));
    }

    void logMessage(double msg){
        logMessage( String.format("%f", msg));
    }

    void logMessage(String msg){
        Log.e( "DETECTOR", msg);
    }

	void setLogoInformation(List<Mat> lLogos){
		
		long startTime = System.nanoTime();
		this.mLogos = lLogos;
		this.descriptorLogos =  new ArrayList<Mat> ();
		this.mokLogos = new ArrayList<MatOfKeyPoint>();
		
		for(int i=0; i < this.mLogos.size(); i++){
			
			this.mokLogos.add(new MatOfKeyPoint());
			this.mDetector.detect(this.mLogos.get(i), this.mokLogos.get(i));
			this.descriptorLogos.add(new Mat());
			this.mExtractor.compute(this.mLogos.get(i), this.mokLogos.get(i), this.descriptorLogos.get(i));

            if (DEBUG) {

                logMessage(String.format("DescriptorLogo %d count %s", i, this.descriptorLogos.get(i).size()));
                logMessage(String.format("KeyPointLogo %d count %s", i,this.mokLogos.get(i).size()));
            }

		}
		
    	long endTime = System.nanoTime();
    	

        if (DEBUG) {
    		for(int i=0; i < this.mLogos.size(); i++){
    			Mat outputLogoImg = new Mat();
    			Features2d.drawKeypoints( this.mLogos.get(i), this.mokLogos.get(i), outputLogoImg);
                SaveImage(outputLogoImg,String.format("name_%d.png",i));
    		}
    	}
	}

    public File getAlbumStorageDir(String albumName) {
        // Get the directory for the user's public pictures directory.
        File file = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), albumName);
        if (!file.mkdirs()) {
            Log.e(TAG, "Directory not created");
        }
        return file;
    }


    public void SaveImage (Mat mat,String filename) {
        Mat mIntermediateMat = new Mat();

        Imgproc.cvtColor(mat, mIntermediateMat, Imgproc.COLOR_RGBA2BGR, 3);

        File path = getAlbumStorageDir("DebugDir");

        File file = new File(path, filename);
        Log.d(TAG,  file.toString());
        Boolean bool = null;
        filename = file.toString();
        bool = Highgui.imwrite(filename, mIntermediateMat);

        if (bool == true)
            Log.d(TAG, "SUCCESS writing image to external storage");
        else
            Log.d(TAG, "Fail writing image to external storage");
    }

	void setSceneInformation(Mat mScene){
        this.mFrame = new Mat();

		long startTime = System.nanoTime();
        Imgproc.cvtColor(mScene, this.mFrame, Imgproc.COLOR_RGBA2RGB);
        long endTime = System.nanoTime();

        if (DEBUG) {
            logMessage(String.format("convert rgba to rgb %d", (endTime - startTime) / 1000000));
        }


		this.mokFrame = new MatOfKeyPoint();
		this.descriptorFrame = new Mat();
		mDetector.detect(this.mFrame, this.mokFrame);
		mExtractor.compute(this.mFrame, this.mokFrame, this.descriptorFrame);
				        
		endTime = System.nanoTime();
			    	
	    if (DEBUG) {
            logMessage(String.format("Compute Scene in ms: %d", (endTime - startTime) / 1000000));
            logMessage(String.format("DescriptorScene count %s", this.descriptorFrame.size().toString() ));
            logMessage(String.format("KeyPointScene count %s", this.mokFrame.size().toString()));

        }
        if (DEBUG_FILE){
			Mat outputLogoImg = new Mat();
			//Features2d.drawKeypoints( mFrame, this.mokFrame, outputLogoImg );

			//Highgui.imwrite(String.format("%s/matches_scene.png", DEBUG_PATH), outputLogoImg);
	    }
	}
	
	int checkHomography(Mat  H)
	{
		
	  double det = H.get(0, 0)[0] * H.get( 1, 1)[0] - H.get( 1, 0)[0] * H.get( 0, 1)[0];
	  if (det < 0)
	  {	
		  if (DEBUG){
			  logMessage(det);
		  }
		  return 1;
	  }
	  
	  
	  double N1 = Math.sqrt(H.get(0, 0)[0] * H.get(0, 0)[0] + H.get( 1, 0)[0] * H.get( 1, 0)[0]);
	  if (N1 > 4 || N1 < 0.1)
	  {
		  if (DEBUG){
			  logMessage(N1);
		  }
		  return 2;
	  }
	  
	  double N2 = Math.sqrt( H.get(  0, 1)[0] *  H.get(  0, 1)[0] + H.get( 1, 1)[0] * H.get( 1, 1)[0]);
	  if (N2 > 4 || N2 < 0.1)
	  {
		  if (DEBUG){
			  logMessage(N2);
		  }
		  return 3;
	  }
	  
	  double N3 = Math.sqrt( H.get(  2, 0)[0] *  H.get( 2, 0)[0] +  H.get(  2, 1)[0] *  H.get(  2, 1)[0]);
	  if (N3 > 0.005) //original 0.02
	  {
		  if (DEBUG){
			  logMessage(N3);
		  }
		  return 4;
	  }
	  return 0;
	}
	
	
	public int detect(Mat mScene, String errorMsg){

		this.setSceneInformation(mScene);

        long startTime = System.nanoTime();


        List<MatOfDMatch> lMatches = new ArrayList<MatOfDMatch>();

		for(int i=0;i<mokLogos.size();i++){
			lMatches.add(new MatOfDMatch());
			//mMatcher.match(descriptorFrame, descriptorLogos.get(i), lMatches.get(i));
			mMatcher.match( descriptorLogos.get(i),descriptorFrame, lMatches.get(i));
		}
		
		List<LinkedList<DMatch>> lGoodMatches = new ArrayList<LinkedList<DMatch>>();
		for(int i=0;i<lMatches.size();i++){
			List<DMatch> matchesList = lMatches.get(i).toList();
			LinkedList<DMatch> goodMatches = new LinkedList<DMatch>();
			
			if (matchesList.size() < 4) {
				lGoodMatches.add(goodMatches);
				continue;
			}			
			Double max_dist = 0.0;
	        Double min_dist = Double.MAX_VALUE;

	        for (int j = 0; j < matchesList.size(); j++) {
	        	Double dist = (double) matchesList.get(j).distance;
	            if (dist < min_dist)
	                min_dist = dist;
	            if (dist > max_dist)
	                max_dist = dist;
	        }
	        
	        for (int j = 0; j < matchesList.size(); j++) {
	            if (matchesList.get(j).distance < (DISTANCE_FAKTOR * min_dist))
	            	goodMatches.addLast(matchesList.get(j));
	        }

            if (DEBUG) {
                logMessage(String.format("logo %d: all/good matches %d/%d - max %f min %f ", i, matchesList.size(), goodMatches.size(), max_dist, min_dist));
            }

            if (goodMatches.size() < 4) {
	        	goodMatches.clear();
	        }
	        
	        lGoodMatches.add(goodMatches);
		}
		if (DEBUG_FILE){
			
			for(int i=0;i<mLogos.size();i++){
				
				Mat outputImg = new Mat();
                Mat newScene = new Mat();
			    MatOfByte drawnMatches = new MatOfByte();
			    MatOfDMatch goodMatches = new MatOfDMatch();
			    goodMatches.fromList(lGoodMatches.get(i));
                //Imgproc.cvtColor(mFrame, newScene, Imgproc.COLOR_RGBA2RGB);
			    if (lGoodMatches.get(i).size() < 4) {
                    if (DEBUG) {
                        logMessage(String.format("No 4 matches for %d", i));
                    }
		        	continue;
		        }
			    Features2d.drawMatches(mLogos.get(i) , mokLogos.get(i), mFrame, mokFrame, goodMatches, outputImg, new Scalar(0, 255, 0), new Scalar(0, 0, 255), drawnMatches, Features2d.NOT_DRAW_SINGLE_POINTS);
			    Highgui.imwrite(String.format("%s/matches_%d_scene.png", DEBUG_PATH, i), outputImg);
			    
			}
		}
		long endTime = System.nanoTime();
     
		List<KeyPoint> keypoints_sceneList = this.mokFrame.toList();
		 
		for(int i=0;i<lGoodMatches.size();i++){
			List<KeyPoint> keypoints_objectList = this.mokLogos.get(i).toList();
		
			LinkedList<DMatch> goodMatches = lGoodMatches.get(i);
	        LinkedList<Point> objList = new LinkedList<Point>();
	        LinkedList<Point> sceneList = new LinkedList<Point>();
	        
			for (int j = 0; j < goodMatches.size(); j++) {
	            objList.addLast(keypoints_objectList.get(goodMatches.get(j).queryIdx).pt);
	            sceneList.addLast(keypoints_sceneList.get(goodMatches.get(j).trainIdx).pt);
	        }
			
	        MatOfPoint2f obj = new MatOfPoint2f();
	        MatOfPoint2f scene = new MatOfPoint2f();
	        
	        obj.fromList(objList);
	        scene.fromList(sceneList);

            if (DEBUG) {

                Mat outputImg = new Mat();
                MatOfByte drawnMatches = new MatOfByte();
                MatOfDMatch good_Matches = new MatOfDMatch();
                good_Matches.fromList(goodMatches);

                Features2d.drawMatches(mLogos.get(i) , mokLogos.get(i), mFrame, mokFrame, good_Matches, outputImg, new Scalar(0, 255, 0), new Scalar(0, 0, 255), drawnMatches, Features2d.NOT_DRAW_SINGLE_POINTS);


                SaveImage ( outputImg, String.format("matches_scene_%d.png", i)) ;

            }
            if(!(scene.size().height > 0 && obj.size().height > 0 && scene.size().height == obj.size().height)){
                logMessage(String.format("findHomo %s - %s",obj.size().toString(),scene.size().toString()));
                continue;
            }
	        Mat H;
	        try {
	    		H = Calib3d.findHomography(obj, scene, Calib3d.RANSAC, 5);
	        } catch (Exception e) {
	        	errorMsg = "error by findHomography\n" + e.getMessage();
	            continue;
	        }
	        
	        endTime = System.nanoTime();
	    	if (DEBUG){
	    		logMessage(String.format("checkHomography %d - Compute in ms: %d" ,checkHomography(H), (endTime - startTime) / 1000000));
	    	}
	    	
	        if ( checkHomography(H) == 0 ){
	        	return i;
	        	//return false;
	        }
	        
	    	
		}



       /*
    	Mat tmp_corners = new Mat(4, 1, CvType.CV_32FC2);
        Mat scene_corners = new Mat(4, 1, CvType.CV_32FC2);

        //get corners from object
        tmp_corners.put(0, 0, new double[]{0, 0});
        tmp_corners.put(1, 0, new double[]{mLogo.cols(), 0});
        tmp_corners.put(2, 0, new double[]{mLogo.cols(), mLogo.rows()});
        tmp_corners.put(3, 0, new double[]{0, mLogo.rows()});
        
        try{
        	Core.perspectiveTransform(tmp_corners, scene_corners, H);
		} catch (Exception e) {
	    	errorMsg = "error by perspectiveTransform\n" + e.getMessage();
	        return false;
	    }
        
        Core.line(mRgba, new Point(scene_corners.get(0, 0)), new Point(scene_corners.get(1, 0)), new Scalar(0, 255, 0), 4);
        Core.line(mRgba, new Point(scene_corners.get(1, 0)), new Point(scene_corners.get(2, 0)), new Scalar(0, 255, 0), 4);
        Core.line(mRgba, new Point(scene_corners.get(2, 0)), new Point(scene_corners.get(3, 0)), new Scalar(0, 255, 0), 4);
        Core.line(mRgba, new Point(scene_corners.get(3, 0)), new Point(scene_corners.get(0, 0)), new Scalar(0, 255, 0), 4);
	    

	    Mat outputImg = new Mat();
	    MatOfByte drawnMatches = new MatOfByte();
	    MatOfDMatch goodMatches = new MatOfDMatch();
	    goodMatches.fromList(good_matches);

	    Features2d.drawMatches(mLogo , mokLogo, mRgba, mokFrame, goodMatches, outputImg, GREEN, RED, drawnMatches, Features2d.NOT_DRAW_SINGLE_POINTS);

	    if (DEBUG){
	        Highgui.imwrite(DEBUG_PATH + "matches_out.png", outputImg);
		    Highgui.imwrite(DEBUG_PATH + "match_out.png", mRgba);
	    }
	    */
		
		return -1;
	}
	
}


