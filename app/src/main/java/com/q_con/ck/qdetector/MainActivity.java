package com.q_con.ck.qdetector;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;


import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;
import android.os.Environment;


import android.content.Intent;


public class MainActivity extends ActionBarActivity implements CvCameraViewListener2 {

    private static final String TAG = "qDetector";

    private CameraBridgeViewBase mOpenCvCameraView;
    private boolean              mIsJavaCamera = false;
    private boolean              mShowWebsite  = true;
    private MenuItem             mItemSwitchCamera = null;
    private MenuItem             mItemSwitchShowWebsite = null;
    private MenuItem             mItemSwitchPrefs = null;

    private Detector             detector;
    private Mat                  mRgba;
    private boolean              takePicture;

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    Log.i(TAG, "OpenCV loaded successfully");
                    mOpenCvCameraView.enableView();
                } break;
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }
        }


    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Log.i(TAG, "called onCreate");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.activity_main);

        mOpenCvCameraView = (CameraBridgeViewBase) findViewById(R.id.java_surface_view);

        mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);

        mOpenCvCameraView.setCvCameraViewListener(this);

        Button btnScan = (Button)findViewById(R.id.scan_btn)        ;
        btnScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takePicture = true;
            }
        });
        detector = new Detector();
        detector.setLogoInformation(loadLogos());

    }

    static{ System.loadLibrary("opencv_java"); }
 /*
    public File getAlbumStorageDir(String albumName) {
        // Get the directory for the user's public pictures directory.
        File file = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), albumName);
        if (!file.mkdirs()) {
            Log.e(TAG, "Directory not created");
        }
        return file;
    }


    public void SaveImage (Mat mat,String filename) {
        Mat mIntermediateMat = new Mat();

        Imgproc.cvtColor(mat, mIntermediateMat, Imgproc.COLOR_RGBA2BGR, 3);

        File path = getAlbumStorageDir("DebugDir");

        File file = new File(path, filename);
        Log.d(TAG,  file.toString());
        Boolean bool = null;
        filename = file.toString();
        bool = Highgui.imwrite(filename, mIntermediateMat);

        if (bool == true)
            Log.d(TAG, "SUCCESS writing image to external storage");
        else
            Log.d(TAG, "Fail writing image to external storage");
    }
*/
    protected List<Mat> loadLogos() {

        List<Mat> lstImg = new ArrayList<Mat> ();

        try{

            Mat rgbQfin = new Mat();
            Imgproc.cvtColor(Utils.loadResource(getApplicationContext(), R.drawable.qfin_logo_200), rgbQfin, Imgproc.COLOR_RGBA2RGB);
            lstImg.add( rgbQfin);

            Mat rgbInit = new Mat();
            Imgproc.cvtColor(Utils.loadResource(getApplicationContext(), R.drawable.t_logo_400), rgbInit, Imgproc.COLOR_BGR2RGB);
            lstImg.add( rgbInit);

            Mat rgbCebit  = new Mat();
            Imgproc.cvtColor(Utils.loadResource(getApplicationContext(), R.drawable.cebit_logo_200), rgbCebit, Imgproc.COLOR_BGR2RGB);

            lstImg.add( rgbCebit);
            /*
            for(int i=0; i < lstImg.size(); i++){


                SaveImage(lstImg.get(i),String.format("source_%d.png",i));
            }
*/
        }catch(Exception e){
            Log.e( TAG, "in loadLogos " + e.toString());
        }

        return lstImg;
    }

    @Override
    public void onPause()
    {
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_9, this, mLoaderCallback);
    }

    public void onDestroy() {
        super.onDestroy();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        Log.i(TAG, "called onCreateOptionsMenu");
         mItemSwitchShowWebsite = menu.add("Open Website/only show finding");
        //mItemSwitchPrefs = menu.add("Settings");
        //getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        String toastMesage = new String();
        Log.i(TAG, "called onOptionsItemSelected; selected item: " + item);

        if (item == mItemSwitchCamera) {
            mOpenCvCameraView.setVisibility(SurfaceView.GONE);
            mIsJavaCamera = !mIsJavaCamera;

            mOpenCvCameraView = (CameraBridgeViewBase) findViewById(R.id.java_surface_view);
            toastMesage = "Java Camera";


            mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
            mOpenCvCameraView.setCvCameraViewListener(this);
            mOpenCvCameraView.enableView();
            Toast toast = Toast.makeText(this, toastMesage, Toast.LENGTH_LONG);
            toast.show();
        }
        else if (item == mItemSwitchShowWebsite) {
            mShowWebsite = !mShowWebsite;
            if(mShowWebsite){
                toastMesage = "Show website";
            }else{
                toastMesage = "Only show hit";
            }
            Toast toast = Toast.makeText(this, toastMesage, Toast.LENGTH_LONG);
            toast.show();
        }else if (item == mItemSwitchPrefs){
            Intent i = new Intent(this, SettingsActivity.class);
            startActivityForResult( i, 1);
        }

        return true;

    }

    public void onCameraViewStarted(int width, int height) {
    }

    public void onCameraViewStopped() {
    }

    public Mat onCameraFrame(CvCameraViewFrame inputFrame) {

        mRgba = inputFrame.rgba();
        //Mat resizeimage = new Mat();
        //Size sz = new Size(mRgba.cols()/2,mRgba.rows()/2);
        //Imgproc.resize( mRgba, resizeimage, sz );

        if(takePicture) {
            takePicture = false;
            try {
                Log.e(TAG, String.format("*********************start ************************"));
                int res = detector.detect(mRgba, "");
                Log.e(TAG, String.format("********************* %d ************************", res));

                switch(res) {
                    case 0: {
                        MainActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast toast = Toast.makeText(MainActivity.this, "Q-fin detected", Toast.LENGTH_LONG);
                                toast.show();
                                if (mShowWebsite) {
                                    Intent i = new Intent(Intent.ACTION_VIEW,
                                            Uri.parse("http://www.q-fin.de"));
                                    startActivity(i);
                                }
                            }
                        });

                        break;
                    }
                    case 1: {

                        MainActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast toast = Toast.makeText(MainActivity.this, "T-Com detected", Toast.LENGTH_LONG);
                                toast.show();
                                if (mShowWebsite) {
                                    Intent i = new Intent(Intent.ACTION_VIEW,
                                            Uri.parse("http://www.telekom.de/"));
                                    startActivity(i);
                                }
                            }
                        });

                        break;
                    }
                    case 2: {
                        MainActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast toast = Toast.makeText(MainActivity.this, "CeBIT detected", Toast.LENGTH_LONG);
                                toast.show();
                                if (mShowWebsite) {
                                    Intent i = new Intent(Intent.ACTION_VIEW,
                                            Uri.parse("http://www.cebit.de"));
                                    startActivity(i);
                                }
                            }
                        });

                        break;
                    }
                    default: {
                        MainActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast toast = Toast.makeText(MainActivity.this, "Nothing found", Toast.LENGTH_LONG);
                                toast.show();
                            }
                    });
                }

                }
            } catch (Exception e) {

                Log.e(TAG, String.format("*******!!!!!!!!!!!!!!******** %s ************************", e.getMessage()));
            }
        }
/*
        Mat mRgbaT = mRgba.t();
        Core.flip(mRgba.t(), mRgbaT,1);
        Imgproc.resize(mRgbaT, mRgbaT,mRgba.size());
*/
        return mRgba;
    }

}
